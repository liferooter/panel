{ pkgs }:
pkgs.mkShell {
  packages = with pkgs; [
    rustc
    cargo
    clippy
    rustfmt
    rust-analyzer
    gdb
    pkg-config
    glib.dev
    gtk4.dev
    libadwaita.dev
    gtk4-layer-shell.dev
  ];
  RUST_BACKTRACE = "full";
}
