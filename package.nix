{ pkgs ? import <nixpkgs> {} }: let
  cargoToml = with builtins; fromTOML (readFile ./Cargo.toml);
in pkgs.rustPlatform.buildRustPackage {
  pname = cargoToml.package.name;

  inherit (cargoToml.package) version;

  src = ./.;

  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "upower_dbus-0.3.2" = "sha256-Bz/bzXCm60AF0inpZJDF4iNZIX3FssImORrE5nZpkyQ=";
    };
  };

  buildInputs = with pkgs; [
    gtk4 gtk4-layer-shell
  ];

  nativeBuildInputs = with pkgs; [
    pkg-config
  ];
}
