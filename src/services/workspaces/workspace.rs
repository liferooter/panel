use glib::prelude::*;
use glib::subclass::prelude::*;

use gtk::glib;

mod imp {
    use super::*;

    use std::{cell::Cell, sync::Mutex};

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Workspace)]
    pub struct Workspace {
        #[property(get, set)]
        name: Mutex<String>,

        #[property(get, set)]
        is_visible: Cell<bool>,

        #[property(get, set)]
        is_focused: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Workspace {
        const NAME: &'static str = "PanelWorkspace";
        type Type = super::Workspace;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Workspace {}
}

glib::wrapper! {
    pub struct Workspace(ObjectSubclass<imp::Workspace>);
}

impl Workspace {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

impl Default for Workspace {
    fn default() -> Self {
        Self::new()
    }
}
