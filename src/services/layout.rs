use futures::prelude::*;
use gio::prelude::*;
use gio::subclass::prelude::*;

use gtk::{gio, glib};

use crate::utils::{sway::requests, Sway};

mod imp {
    use super::*;

    use crate::utils::{
        sway::{
            events::{self, InputChange},
            requests::GetInputs,
        },
        Sway,
    };

    use std::{cell::RefCell, pin::pin};

    use anyhow::{Context, Result};

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Layout)]
    pub struct Layout {
        #[property(get)]
        layout: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Layout {
        const NAME: &'static str = "PanelLayout";
        type Type = super::Layout;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Layout {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj().clone();
            glib::MainContext::default().spawn_local(
                async move {
                    let mut sway = Sway::new().await?;
                    let imp = obj.imp();

                    let mut events = pin!(Sway::new().await?.subscribe::<events::Input>().await?);

                    let layout = sway
                        .send(GetInputs)
                        .await?
                        .into_iter()
                        .find_map(|input| input.xkb_active_layout_name)
                        .context("No keyboard on this computer")?;

                    imp.layout.replace(layout);
                    obj.notify_layout();

                    loop {
                        let event: events::Input = events
                            .try_next()
                            .await?
                            .context("Failed to receive an event from Sway")?;

                        if let InputChange::XkbKeymap | InputChange::XkbLayout = event.change {
                            imp.layout.replace(
                                event
                                    .input
                                    .xkb_active_layout_name
                                    .context("A keyboard has no layout")?,
                            );
                            obj.notify_layout();
                        }
                    }
                }
                .map(|res: Result<()>| res.unwrap()),
            );
        }
    }
}

glib::wrapper! {
    pub struct Layout(ObjectSubclass<imp::Layout>);
}

impl Layout {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn toggle(&self) {
        glib::MainContext::default().spawn_local(
            async {
                Sway::new()
                    .await?
                    .send(requests::RunCommand(String::from(
                        "input * xkb_switch_layout next",
                    )))
                    .await
            }
            .map(|res| res.unwrap()),
        );
    }
}

impl Default for Layout {
    fn default() -> Self {
        Layout::new()
    }
}
