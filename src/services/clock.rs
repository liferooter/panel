use glib::prelude::*;
use glib::subclass::prelude::*;

use gtk::glib;

mod imp {
    use super::*;
    use chrono::prelude::*;

    use std::{cell::Cell, time::Duration};

    use chrono::Local;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Clock)]
    pub struct Clock {
        #[property(get)]
        year: Cell<i32>,

        #[property(get)]
        month: Cell<u32>,

        #[property(get)]
        day: Cell<u32>,

        #[property(get)]
        hour: Cell<u32>,

        #[property(get)]
        minute: Cell<u32>,

        #[property(get)]
        second: Cell<u32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Clock {
        const NAME: &'static str = "PanelClock";
        type Type = super::Clock;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Clock {
        fn constructed(&self) {
            self.parent_constructed();

            self.update_clock();

            let obj = self.obj().downgrade();
            glib::timeout_add_local(Duration::from_millis(500), move || {
                let Some(clock) = obj.upgrade() else {
                    return glib::ControlFlow::Break;
                };

                clock.imp().update_clock();

                glib::ControlFlow::Continue
            });
        }
    }

    impl Clock {
        fn update_clock(&self) {
            let obj = self.obj();
            let date = Local::now();

            if date.year() != self.year.get() {
                self.year.set(date.year());
                obj.notify_year();
            }

            if date.month() != self.month.get() {
                self.month.set(date.month());
                obj.notify_month();
            }

            if date.day() != self.day.get() {
                self.day.set(date.day());
                obj.notify_day();
            }

            let time = date.time();

            if time.hour() != self.hour.get() {
                self.hour.set(time.hour());
                obj.notify_hour();
            }

            if time.minute() != self.minute.get() {
                self.minute.set(time.minute());
                obj.notify_minute();
            }

            if time.second() != self.second.get() {
                self.second.set(time.second());
                obj.notify_second();
            }
        }
    }
}

glib::wrapper! {
    pub struct Clock(ObjectSubclass<imp::Clock>);
}

impl Clock {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

impl Default for Clock {
    fn default() -> Self {
        Self::new()
    }
}
