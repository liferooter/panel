use futures::prelude::*;
use glib::prelude::*;
use glib::subclass::prelude::*;

use gtk::glib;

mod imp {
    use super::*;

    use std::cell::Cell;

    use anyhow::{Context, Result};
    use upower_dbus::{BatteryState, UPowerProxy};

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Battery)]
    pub struct Battery {
        #[property(get)]
        percentage: Cell<f64>,

        #[property(get)]
        is_charging: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Battery {
        const NAME: &'static str = "PanelBattery";
        type Type = super::Battery;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Battery {
        fn constructed(&self) {
            self.parent_constructed();

            glib::MainContext::default()
                .spawn_local(Self::async_constructed(self.obj().clone()).map(|res| res.unwrap()));
        }
    }

    impl Battery {
        async fn async_constructed(obj: super::Battery) -> Result<()> {
            let imp = obj.imp();

            let connection = zbus::Connection::system().await?;
            let upower = UPowerProxy::new(&connection).await?;
            let device = upower.get_display_device().await?;

            let mut stream = futures::stream_select!(
                device.receive_percentage_changed().await.map(|_| ()),
                device.receive_state_changed().await.map(|_| ())
            );

            loop {
                imp.percentage.set(device.percentage().await?);
                obj.notify_percentage();
                imp.is_charging.set(matches!(
                    device.state().await?,
                    BatteryState::Charging | BatteryState::FullyCharged
                ));
                obj.notify_is_charging();

                stream
                    .next()
                    .await
                    .context("Failed to receive property change from UPower")?;
            }
        }
    }
}

glib::wrapper! {
    pub struct Battery(ObjectSubclass<imp::Battery>);
}

impl Battery {
    pub fn new() -> Self {
        glib::Object::new()
    }
}

impl Default for Battery {
    fn default() -> Self {
        Battery::new()
    }
}
