use futures::prelude::*;
use gio::prelude::*;
use gio::subclass::prelude::*;

use gtk::{gio, glib};

pub use workspace::Workspace;

mod workspace;

mod imp {
    use super::*;

    use crate::utils::{
        sway::{events, requests::GetWorkspaces},
        Sway,
    };

    use anyhow::{Context, Result};

    use std::{
        cell::OnceCell,
        cmp::Ordering,
        pin::pin,
        sync::{Mutex, MutexGuard},
    };

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Workspaces)]
    pub struct Workspaces {
        #[property(get, construct_only)]
        output: OnceCell<String>,

        workspaces: Mutex<Vec<Workspace>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Workspaces {
        const NAME: &'static str = "PanelWorkspaces";
        type Type = super::Workspaces;
        type Interfaces = (gio::ListModel,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for Workspaces {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj().clone();
            glib::MainContext::default().spawn_local(
                async move {
                    let mut sway = Sway::new().await?;
                    let imp = obj.imp();
                    let output = obj
                        .imp()
                        .output
                        .get()
                        .context("Failed to get current output")?;

                    let mut events =
                        pin!(Sway::new().await?.subscribe::<events::Workspace>().await?);

                    loop {
                        let workspaces = sway
                            .send(GetWorkspaces)
                            .await?
                            .into_iter()
                            .filter(|ws| &ws.output == output)
                            .collect::<Vec<_>>();

                        let old_length;
                        let new_length;

                        {
                            let mut items = imp.items();

                            old_length = items.len();

                            while items.len() < workspaces.len() {
                                items.push(Workspace::new());
                            }
                            while items.len() > workspaces.len() {
                                items.pop();
                            }

                            for (item, ws) in items.iter().zip(workspaces) {
                                item.set_name(&*ws.name);
                                item.set_is_visible(ws.visible);
                                item.set_is_focused(ws.focused);
                            }

                            new_length = items.len();
                        }

                        match new_length.cmp(&old_length) {
                            Ordering::Less => {
                                let diff = old_length - new_length;
                                obj.items_changed(new_length.try_into()?, diff.try_into()?, 0);
                            }
                            Ordering::Greater => {
                                let diff = new_length - old_length;
                                obj.items_changed(old_length.try_into()?, 0, diff.try_into()?);
                            }
                            Ordering::Equal => (),
                        }

                        events
                            .try_next()
                            .await?
                            .context("Failed to receive an event from Sway")?;
                    }
                }
                .map(|res: Result<()>| res.unwrap()),
            );
        }
    }

    impl ListModelImpl for Workspaces {
        fn item_type(&self) -> glib::Type {
            Workspace::static_type()
        }

        fn n_items(&self) -> u32 {
            self.items().len().try_into().unwrap()
        }

        fn item(&self, position: u32) -> Option<glib::Object> {
            self.items()
                .get(usize::try_from(position).unwrap())
                .cloned()
                .and_upcast()
        }
    }

    impl Workspaces {
        fn items(&self) -> MutexGuard<Vec<Workspace>> {
            self.workspaces.lock().unwrap()
        }
    }
}

glib::wrapper! {
    pub struct Workspaces(ObjectSubclass<imp::Workspaces>)
        @implements gio::ListModel;
}

impl Workspaces {
    pub fn new(output: &str) -> Self {
        glib::Object::builder().property("output", output).build()
    }
}
