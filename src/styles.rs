use gtk::gdk;

pub fn setup() {
    gtk::Settings::default()
        .expect("Failed to get GTK settings")
        .set_gtk_theme_name(Some("Empty"));

    let style_provider = gtk::CssProvider::new();
    style_provider.load_from_string(grass::include!("style.scss"));

    gtk::style_context_add_provider_for_display(
        gdk::Display::default().as_ref().unwrap(),
        &style_provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}
