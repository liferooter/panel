use gtk::prelude::*;
use gtk::subclass::prelude::*;

use gtk::glib;

mod imp {
    use std::cell::OnceCell;

    use super::*;

    use crate::{
        services::*,
        utils::{sway::requests, Sway},
    };

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Content)]
    pub struct Content {
        #[property(get, construct_only)]
        output: OnceCell<String>,

        clock: Clock,
        battery: Battery,
        layout: Layout,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Content {
        const NAME: &'static str = "PanelContent";
        type Type = super::Content;
        type ParentType = gtk::Widget;

        fn class_init(class: &mut Self::Class) {
            class.set_css_name("content");
            class.set_layout_manager_type::<gtk::BinLayout>()
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Content {
        fn constructed(&self) {
            self.parent_constructed();

            self.setup_fonts();

            let clock = gtk::Label::new(None);

            gtk::ClosureExpression::new::<String>(
                [
                    gtk::PropertyExpression::new(
                        Clock::static_type(),
                        Some(gtk::ObjectExpression::new(&self.clock)),
                        "hour",
                    ),
                    gtk::PropertyExpression::new(
                        Clock::static_type(),
                        Some(gtk::ObjectExpression::new(&self.clock)),
                        "minute",
                    ),
                ],
                glib::closure!(|_: glib::Object, hour: u32, minute: u32| format!(
                    "{hour:02}:{minute:02}"
                )),
            )
            .bind(&clock, "label", Some(&self.clock));

            gtk::ClosureExpression::new::<String>(
                [
                    gtk::PropertyExpression::new(
                        Clock::static_type(),
                        Some(gtk::ObjectExpression::new(&self.clock)),
                        "day",
                    ),
                    gtk::PropertyExpression::new(
                        Clock::static_type(),
                        Some(gtk::ObjectExpression::new(&self.clock)),
                        "month",
                    ),
                    gtk::PropertyExpression::new(
                        Clock::static_type(),
                        Some(gtk::ObjectExpression::new(&self.clock)),
                        "year",
                    ),
                ],
                glib::closure!(|_: glib::Object, day: u32, month: u32, year: i32| format!(
                    "{day}/{month}/{year}"
                )),
            )
            .bind(&clock, "tooltip-text", Some(&self.clock));

            let workspaces_model = Workspaces::new(&self.obj().output());
            let workspace_factory = gtk::SignalListItemFactory::new();

            workspace_factory.connect_setup(|_, item| {
                let item = item.downcast_ref::<gtk::ListItem>().unwrap();
                let label = gtk::Label::new(None);

                label.add_css_class("workspace");
                let controller = gtk::GestureClick::new();
                controller.connect_pressed(glib::clone!(@weak item => move |_, _, _, _| {
                    let ws = item.item().and_downcast::<Workspace>().unwrap();
                    glib::MainContext::default()
                        .spawn_local(async move {
                            Sway::new().await.unwrap().send(requests::RunCommand(format!("workspace {}", ws.name()))).await.unwrap();
                        });
                }));
                label.add_controller(controller);

                item.set_child(Some(&label));
            });

            workspace_factory.connect_bind(|_, item| {
                let item = item.downcast_ref::<gtk::ListItem>().unwrap();
                let label = item.child().and_downcast::<gtk::Label>().unwrap();
                let ws = item.item().and_downcast::<Workspace>().unwrap();

                ws.bind_property("name", &label, "label")
                    .sync_create()
                    .build();

                let update_classes = glib::clone!(@weak label => move |ws: &Workspace| {
                    let classes = [
                        (ws.is_visible(), "visible"),
                        (ws.is_focused(), "focused"),
                    ];

                    for (use_class, class) in classes {
                        if use_class {
                            label.add_css_class(class);
                        } else {
                            label.remove_css_class(class);
                        }
                    }
                });

                update_classes(&ws);
                ws.connect_is_visible_notify(update_classes.clone());
                ws.connect_is_focused_notify(update_classes);
            });

            let workspaces = gtk::ListView::builder()
                .model(&gtk::NoSelection::new(Some(workspaces_model)))
                .orientation(gtk::Orientation::Horizontal)
                .factory(&workspace_factory)
                .build();

            let battery_label = gtk::Label::new(None);
            battery_label.add_css_class("battery");
            self.battery
                .bind_property("percentage", &battery_label, "label")
                .transform_to(|_, percentage: f64| Some(format!("󱐋{}%", percentage.round() as i32)))
                .sync_create()
                .build();

            let update_battery_style = |battery: &Battery, label: &gtk::Label| {
                if battery.is_charging() {
                    label.add_css_class("charging");
                } else {
                    label.remove_css_class("charging");
                }
            };

            update_battery_style(&self.battery, &battery_label);
            self.battery.connect_is_charging_notify(
                glib::clone!(@weak battery_label => move |battery| {
                    update_battery_style(battery, &battery_label);
                }),
            );

            let layout_label = gtk::Label::new(None);
            layout_label.add_css_class("layout");

            let layout_click = gtk::GestureClick::new();
            layout_click.connect_pressed(
                glib::clone!(@weak self.layout as layout => move |_, _, _, _| {
                    layout.toggle();
                }),
            );
            layout_label.add_controller(layout_click);

            self.layout
                .bind_property("layout", &layout_label, "label")
                .sync_create()
                .transform_to(|_, name: String| {
                    Some(
                        name.chars()
                            .flat_map(char::to_lowercase)
                            .take(2)
                            .collect::<String>(),
                    )
                })
                .build();

            let right_widgets = gtk::Box::new(gtk::Orientation::Horizontal, 0);
            right_widgets.add_css_class("container");
            right_widgets.append(&layout_label);
            right_widgets.append(&battery_label);

            let center_box = gtk::CenterBox::builder()
                .start_widget(&workspaces)
                .center_widget(&clock)
                .end_widget(&right_widgets)
                .build();

            center_box.set_parent(&*self.obj());
        }
    }

    impl WidgetImpl for Content {}

    impl Content {
        fn setup_fonts(&self) {}
    }
}

glib::wrapper! {
    pub struct Content(ObjectSubclass<imp::Content>)
        @extends gtk::Widget;
}

impl Content {
    pub fn new(output: &str) -> Self {
        glib::Object::builder().property("output", output).build()
    }
}
