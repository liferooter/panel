use futures::prelude::*;
use gtk::prelude::*;

use utils::{
    sway::{events, requests::GetOutputs},
    Sway,
};

use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    pin::pin,
};

use anyhow::{Context, Result};
use gtk::{gdk, gio, glib};
use gtk4_layer_shell::{Edge, Layer, LayerShell};

mod services;
mod styles;
mod utils;
mod widgets;

fn main() -> glib::ExitCode {
    tracing_subscriber::fmt::init();

    let application = gtk::Application::new(
        Some("io.gitlab.liferooter.panel"),
        gio::ApplicationFlags::empty(),
    );

    application.set_default();

    application.connect_startup(|_| {
        styles::setup();
    });

    let start = std::sync::Once::new();
    application.connect_activate(move |app| {
        let app = app.to_owned();
        start.call_once(move || {
            std::mem::forget(app.hold());
            glib::MainContext::default().spawn_local(
                async move {
                    let mut bars: HashMap<String, gtk::ApplicationWindow> = HashMap::new();
                    let mut sway = Sway::new().await?;
                    let mut events = pin!(Sway::new().await?.subscribe::<events::Output>().await?);

                    loop {
                        let outputs = sway
                            .send(GetOutputs)
                            .await?
                            .into_iter()
                            .filter_map(|output| output.active.then_some(output.name))
                            .collect::<HashSet<_>>();

                        let removed_outputs = bars
                            .keys()
                            .filter(|&output| !outputs.contains(output))
                            .cloned()
                            .collect::<Vec<_>>();

                        for output in removed_outputs {
                            if let Some(window) = bars.remove(&output) {
                                window.destroy();
                            }
                        }

                        for output in outputs {
                            if let Entry::Vacant(entry) = bars.entry(output.clone()) {
                                let window = gtk::ApplicationWindow::builder()
                                    .application(&app)
                                    .default_height(40)
                                    .child(&widgets::Content::new(&output))
                                    .build();

                                window.add_css_class("panel");

                                window.init_layer_shell();
                                window.set_namespace("panel");
                                window.set_layer(Layer::Top);
                                window.set_exclusive_zone(36);
                                window.set_monitor(
                                    &gdk::Display::default()
                                        .context("Failed to get default display")?
                                        .monitors()
                                        .into_iter()
                                        .map(|res| res.unwrap().downcast::<gdk::Monitor>().unwrap())
                                        .find(|monitor| {
                                            monitor.connector().as_deref() == Some(&output)
                                        })
                                        .context("Failed to find monitor for output")?,
                                );

                                let anchors = [Edge::Left, Edge::Right, Edge::Top];

                                for anchor in anchors {
                                    window.set_anchor(anchor, true);
                                }

                                window.present();
                                entry.insert(window);
                            }
                        }

                        events
                            .try_next()
                            .await?
                            .context("Failed to receive output event from Sway")?;
                    }
                }
                .map(|res: Result<()>| res.unwrap()),
            );
        })
    });

    application.run()
}
