use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct Workspace {
    pub num: i32,
    pub name: String,
    #[serde(default)]
    pub visible: bool,
    pub focused: bool,
    pub rect: Rect,
    pub output: String,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub width: u32,
    pub height: u32,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Output {
    pub name: String,
    pub make: String,
    pub model: String,
    pub serial: String,
    pub active: bool,
    pub power: bool,
    #[serde(default = "_default_scale")]
    pub scale: f64,
    #[serde(default)]
    pub subpixel_hinting: SubpixelHinting,
    #[serde(default)]
    pub transform: OutputTransform,
    pub current_workspace: Option<String>,
    pub modes: Vec<OutputMode>,
    #[serde(default)]
    pub current_mode: Option<OutputMode>,
    pub rect: Rect,
}

fn _default_scale() -> f64 {
    1.0
}

#[derive(Debug, Default, Clone, Copy, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum SubpixelHinting {
    Rgb,
    Bgr,
    Vrgb,
    Vbgr,
    #[default]
    Unknown,
    None,
}

#[derive(Debug, Default, Clone, Copy, Deserialize)]
pub enum OutputTransform {
    #[default]
    #[serde(rename = "normal")]
    Normal,
    #[serde(rename = "90")]
    Deg90,
    #[serde(rename = "180")]
    Deg180,
    #[serde(rename = "270")]
    Deg270,
    #[serde(rename = "flipped-90")]
    Flipped90,
    #[serde(rename = "flipped-180")]
    Flipped180,
    #[serde(rename = "flipped-270")]
    Flipped270,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct OutputMode {
    pub width: u32,
    pub height: u32,
    pub refresh: u32,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Input {
    pub identifier: String,
    pub name: String,
    pub vendor: i32,
    pub product: i32,
    #[serde(rename = "type")]
    pub input_type: InputType,
    #[serde(default)]
    pub xkb_active_layout_name: Option<String>,
    #[serde(default)]
    pub xkb_layout_names: Option<Vec<String>>,
    #[serde(default)]
    pub xkb_active_layout_index: Option<usize>,
    #[serde(default)]
    pub scroll_factor: Option<f64>,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum InputType {
    Keyboard,
    Pointer,
    Touch,
    TabletTool,
    TabletPad,
    Touchpad,
    Switch,
}
