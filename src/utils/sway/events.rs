use serde::{Deserialize, Serialize};

use super::types;

pub trait Event: for<'a> Deserialize<'a> {
    const TYPE: EventType;
    const ID: i32;
}

#[allow(unused)]
#[derive(Debug, Clone, Copy, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum EventType {
    Workspace,
    Output,
    Mode,
    Window,
    BarconfigUpdate,
    Binding,
    Shutdown,
    Tick,
    BarStateUpdate,
    Input,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Workspace {
    pub change: WorkspaceChange,
    #[serde(default)]
    pub current: Option<types::Workspace>,
    #[serde(default)]
    pub old: Option<types::Workspace>,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum WorkspaceChange {
    Init,
    Empty,
    Focus,
    Move,
    Rename,
    Urgent,
    Reload,
}

impl Event for Workspace {
    const TYPE: EventType = EventType::Workspace;
    const ID: i32 = 0x80000000u32 as i32;
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct Output {}

impl Event for Output {
    const TYPE: EventType = EventType::Output;
    const ID: i32 = 0x80000001u32 as i32;
}

#[derive(Debug, Clone, Deserialize)]
pub struct Input {
    pub change: InputChange,
    pub input: types::Input,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum InputChange {
    Added,
    Removed,
    XkbKeymap,
    XkbLayout,
    LibinputConfig,
}

impl Event for Input {
    const TYPE: EventType = EventType::Input;
    const ID: i32 = 0x80000015u32 as i32;
}
