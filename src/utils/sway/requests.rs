use serde::Deserialize;

use super::{events, types};

pub trait Request: Sized {
    type Response: for<'a> Deserialize<'a>;

    const PAYLOAD_TYPE: i32;

    fn into_bytes(self) -> Vec<u8> {
        Vec::new()
    }
}

/// Parses and runs the payload as sway commands.
#[derive(Debug, Clone)]
pub struct RunCommand(pub String);

#[derive(Debug, Clone, Deserialize)]
pub struct RunCommandResponse {
    pub success: bool,
    #[serde(default)]
    pub parse_error: bool,
    #[serde(default)]
    pub error: Option<String>,
}

impl Request for RunCommand {
    type Response = Vec<RunCommandResponse>;

    const PAYLOAD_TYPE: i32 = 0;

    fn into_bytes(self) -> Vec<u8> {
        self.0.into_bytes()
    }
}

/// Retrieves the list of workspaces.
#[derive(Debug, Clone, Copy)]
pub struct GetWorkspaces;

impl Request for GetWorkspaces {
    type Response = Vec<types::Workspace>;

    const PAYLOAD_TYPE: i32 = 1;
}

#[derive(Debug, Clone)]
pub struct Subscribe {
    pub event_types: Vec<events::EventType>,
}

#[derive(Debug, Clone, Copy, Deserialize)]
pub struct SubscribeResponse {
    pub success: bool,
}

impl Request for Subscribe {
    type Response = SubscribeResponse;

    const PAYLOAD_TYPE: i32 = 2;

    fn into_bytes(self) -> Vec<u8> {
        serde_json::to_vec(&self.event_types).unwrap()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct GetOutputs;

impl Request for GetOutputs {
    type Response = Vec<types::Output>;

    const PAYLOAD_TYPE: i32 = 3;
}

#[derive(Debug, Clone, Copy)]
pub struct GetInputs;

impl Request for GetInputs {
    type Response = Vec<types::Input>;

    const PAYLOAD_TYPE: i32 = 100;
}
