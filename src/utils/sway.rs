use smol::prelude::*;

use futures::TryFutureExt;

use anyhow::{bail, Result};
use smol::net::unix::UnixStream;

use self::{
    events::Event,
    requests::{Request, Subscribe},
};

pub mod events;
pub mod requests;
pub mod types;

#[derive(Debug)]
pub struct Sway {
    socket: UnixStream,
}

const MAGIC_STRING: &[u8; 6] = b"i3-ipc";

impl Sway {
    pub async fn new() -> Result<Self> {
        let socket_path = std::env::var("SWAYSOCK")?;
        let socket = UnixStream::connect(socket_path).await?;

        Ok(Self { socket })
    }

    pub async fn send<R: Request>(&mut self, request: R) -> Result<R::Response> {
        self.socket.write(MAGIC_STRING).await?;

        let payload = request.into_bytes();

        self.socket
            .write(&i32::try_from(payload.len())?.to_ne_bytes())
            .await?;
        self.socket.write(&R::PAYLOAD_TYPE.to_ne_bytes()).await?;
        self.socket.write(&payload).await?;

        let (reply_type, reply) = self.read_reply().await?;

        if reply_type != R::PAYLOAD_TYPE {
            bail!(
                "Reply type is not equal to request type: expected {}, found {}",
                R::PAYLOAD_TYPE,
                reply_type
            );
        }

        Ok(serde_json::from_slice(&reply)?)
    }

    pub async fn subscribe<E: Event>(mut self) -> Result<impl Stream<Item = Result<E>>> {
        let response = self
            .send(Subscribe {
                event_types: vec![E::TYPE],
            })
            .await?;

        if !response.success {
            bail!("Failed to subsribe to events of type {:?}", E::TYPE);
        }

        Ok(smol::stream::try_unfold(self, move |mut sway| {
            async {
                let (id, event) = sway.read_reply().await?;

                if id != E::ID {
                    return Ok((None, sway));
                }

                Ok((Some(serde_json::from_slice(&event)?), sway))
            }
            .map_ok(Some)
        })
        .filter_map(|res| res.transpose()))
    }

    async fn read_reply(&mut self) -> Result<(i32, Vec<u8>)> {
        let mut reply_magic = [0u8; MAGIC_STRING.len()];
        self.socket.read_exact(&mut reply_magic).await?;

        if &reply_magic != MAGIC_STRING {
            bail!(
                "Reply doesn't start with magic string: expected {:?}, found {:?}",
                MAGIC_STRING,
                reply_magic
            );
        }

        let mut reply_size = [0u8; 4];
        self.socket.read_exact(&mut reply_size).await?;
        let reply_size = usize::try_from(i32::from_ne_bytes(reply_size))?;

        let mut reply_type = [0u8; 4];
        self.socket.read_exact(&mut reply_type).await?;
        let reply_type = i32::from_ne_bytes(reply_type);

        let mut reply_payload = vec![0u8; reply_size];
        self.socket.read_exact(&mut reply_payload).await?;

        Ok((reply_type, reply_payload))
    }
}
